#!/bin/sh

VER=$1

mkdir rex-io-server-plugin-reports-$VER
cp -R lib rex-io-server-plugin-reports-$VER

tar czf rex-io-server-plugin-reports-$VER.tar.gz rex-io-server-plugin-reports-$VER

rm -rf rex-io-server-plugin-reports-$VER
