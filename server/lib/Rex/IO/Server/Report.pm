#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Server::Report;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON;
use Mango;
use Digest::MD5 qw(md5_hex);

use Data::Dumper;
use Rex::IO::Server::Deploy;



sub store {
   my ($self) = @_;

   eval {
      my $ref = {
         server_name => $self->param("server"),
         report      => $self->req->json,
         date        => time(),
         service     => $self->param("service"),
         task        => $self->param("task"),
         _id         => md5_hex($self->param("server") . "-" . time() . "-" . $$ . "-" . rand(9000000)),
      };

      $self->_mango->db("rexio")->collection("report")->insert($ref);
   } or do {
      $self->app->log->debug("error inserting data into mongodb: $@");
      return $self->render(json => {ok => Mojo::JSON->false, error => $@}, status => 500);
   };

   return $self->render(json => {ok => Mojo::JSON->true});
}

sub search { 
   my ($self) = @_;

}

sub list_reports_for_server {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("server"));
   if(! $hw) {
      return $self->render(json => {ok => Mojo::JSON->false, error => "server not found"}, status => 404);
   }

   my $docs = $self->_mango->db("rexio")->collection("report")->find({server_name => $hw->name})->sort({date => -1})->limit(10)->all;

   my @ret;
   for my $doc (@{ $docs }) {
      push @ret, { id => $doc->{_id}, server_name => $doc->{server_name}, time => $doc->{date}, service => $doc->{service}, task => $doc->{task} };
   }

   return $self->render(json => {ok => Mojo::JSON->true, data => \@ret});
}

sub get_report {
   my ($self) = @_;

   my $doc = $self->_mango->db("rexio")->collection("report")->find_one({_id => $self->param("id")});

   if(! $doc) {
      return $self->render(json => {ok => Mojo::JSON->false, error => "No report found."}, status => 404);
   }

   return $self->render(json => {ok => Mojo::JSON->true, data => $doc});
}

sub delete_report {
   my ($self) = @_;

   eval {
      $self->_mango->db("rexio")->collection("report")->remove({_id => $self->param("id")});
   } or do {
      return $self->render(json => {ok => Mojo::JSON->false, error => "Error removing report"}, status => 500);
   };

   return $self->render(json => {ok => Mojo::JSON->true});
}

sub get_status_of_last_report {
   my ($self) = @_;

   #  db.report.find({"server_name":"app-demo3"}).sort({"date": 1}).limit(1)
   my $docs = $self->_mango->db("rexio")->collection("report")->find({ server_name => $self->param("server") })->sort({date => -1})->limit(1)->all;

   my $doc = $docs->[0];

   if(! $doc) {
      return $self->render(json => {ok => Mojo::JSON->false, error => "Error getting last report"}, status => 404);
   }

   my @reports = @{ $doc->{report} };
   my $failed = 0;
   for my $rep (@reports) {
      if(exists $rep->{success} && $rep->{success} != 1) {
          return $self->render(json => {ok => Mojo::JSON->false}, status => 500);
      }
   }

   return $self->render(json => {ok => Mojo::JSON->true});
}

sub cleanup_old_reports {
   my ($self) = @_;

   my $timestamp = $self->param("timestamp");

   if($timestamp !~ m/^\d+$/) {
      return $self->render(json => {ok => Mojo::JSON->false, error => "timestamp must be an integer (epoch time)"}, status => 500);
   }

   eval {
      $self->_mango->db("rexio")->collection("report")->remove({
            date => {
               '$lte' => $timestamp,
            }
         });
   } or do {
      return $self->render(json => {ok => Mojo::JSON->false, error => "something went wrong"}, status => 500);
   };

   return $self->render(json => {ok => Mojo::JSON->true});
}

sub __delete_hardware__ {
   my ($self, $mojo, $hw) = @_;

   my $name = $hw->name;
   eval {
      $self->_mango->db("rexio")->collection("report")->remove({server_name => $name});
   } or do {
      $mojo->app->log->info("error removing hardware from mongodb: $name ($@)");
   };
}

sub __register__ {
   my ($self, $app) = @_;
   my $r = $app->routes;

   $r->post("/1.0/report/:server/:service/:task")->to("report#store");
   $r->get("/1.0/report")->to("report#search");
   $r->route("/1.0/report/:server")->via("LIST")->to("report#list_reports_for_server");
   $r->get("/1.0/report/:id")->to("report#get_report");
   $r->delete("/1.0/report/:id")->to("report#delete_report");
   $r->get("/1.0/report/last/status/:server")->to("report#get_status_of_last_report");
   $r->delete("/1.0/report/cleanup/:timestamp")->to("report#cleanup_old_reports");


   Rex::IO::Server::Deploy->register_hook(installation_finished => sub {
      my ($ctrl, %opt) = @_;

      my $hw = $opt{hardware};

      eval {
         my $ref = {
            server_name => $hw->name,
            report      => [],
            date        => time(),
            service     => "Rex.IO",
            task        => "OS installation",
            _id         => md5_hex($hw->name . "-" . time() . "-" . $$ . "-" . rand(9000000)),
         };

         $self->_mango->db("rexio")->collection("report")->insert($ref);
      } or do {
         $self->app->log->debug("error inserting data into mongodb: $@");
      };
   });
}

my $mango;
sub _mango {
   if($mango) { return $mango; }
   $mango = Mango->new('mongodb://localhost:27017');
}

1;
