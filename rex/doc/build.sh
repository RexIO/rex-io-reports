#!/bin/sh

VER=$1

mkdir rex-report-rexio-$VER
cp -R lib rex-report-rexio-$VER

tar czf rex-report-rexio-$VER.tar.gz rex-report-rexio-$VER

rm -rf rex-report-rexio-$VER
