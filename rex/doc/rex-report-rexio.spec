Summary: Rex Reporting Module for Rex.IO
Name: rex-report-rexio
Version: 0.2.13
Release: 1
License: Apache 2.0
Group: Utilities/System
Source: http://rex.io/downloads/rex-report-rexio-0.2.13.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
AutoReqProv: no

BuildRequires: rexio-perl >= 5.18.0
BuildRequires: rexio-perl-Mojolicious
#Requires: libssh2 >= 1.2.8 - is included in perl-Net-SSH2 deps
Requires: rexio-perl >= 5.18.0
Requires: rexio-perl-Mojolicious

%description
Reporting Module for Rex, to report to Rex.IO

%prep
%setup -n %{name}-%{version}


%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}/usr/lib/rexio/perl/lib/site_perl/5.18.0/Rex/Report
%{__cp} -R lib/Rex/Report/RexIO.pm %{buildroot}/usr/lib/rexio/perl/lib/site_perl/5.18.0/Rex/Report/

### Clean up buildroot
find %{buildroot} -name .packlist -exec %{__rm} {} \;

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
/usr/lib/rexio/perl/lib/site_perl/5.18.0/Rex/Report

%changelog

* Thu Jul 25 2013 Jan Gehring <jan.gehring at, gmail.com> 0.2.13-1
- initial packaged

