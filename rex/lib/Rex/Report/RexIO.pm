#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::Report::RexIO;

use strict;
use warnings;

use Data::Dumper;
use Rex::Report::YAML;
use base qw(Rex::Report::YAML);
use Mojo::UserAgent;
use Mojo::JSON;
require Rex::Logger;

our $REXIO_SERVER = "http://localhost:5000/1.0/report";

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = $proto->SUPER::new(@_);

   bless($self, $proto);

   $self->{__reports__} = [];

   return $self;
}

sub write_report {
   my ($self) = @_;

   my $server_name = Rex::Commands::connection->server;

   my $ua = Mojo::UserAgent->new;
   my $ref = $ua->post($REXIO_SERVER . "/$server_name/$ENV{REX_SERVICE}/$ENV{REX_TASK}", json => $self->{__reports__})->res->json;

   if($ref->{ok} != Mojo::JSON->true) {
      Rex::Logger::info("Unable to send reporting to server: $REXIO_SERVER", "warn");
   }

   $self->{__reports__} = [];
}

1;
