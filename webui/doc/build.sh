#!/bin/sh

VER=$1

mkdir rex-io-webui-plugin-reports-$VER
cp -R lib rex-io-webui-plugin-reports-$VER

tar czf rex-io-webui-plugin-reports-$VER.tar.gz rex-io-webui-plugin-reports-$VER

rm -rf rex-io-webui-plugin-reports-$VER
