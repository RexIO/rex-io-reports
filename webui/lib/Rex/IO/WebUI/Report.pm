#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::WebUI::Report;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON;
use Mango;
use Digest::MD5 qw(md5_hex);
use DateTime;
use YAML;

use File::Basename 'dirname';

use Data::Dumper;

sub view {
   my ($self) = @_;

   my $ret = $self->rexio->call(GET => "1.0", "report", $self->param("report_id") => undef);
   my $server = $self->rexio->get_server($self->param("server_id"));

   my @parts = map {
        $_->{start_time} = DateTime->from_epoch(epoch => $_->{start_time});
        $_->{end_time}   = DateTime->from_epoch(epoch => $_->{end_time});
        if(exists $_->{data}) {
           my @yaml_rep = split(/\n/, Dump($self->_custom_format($_->{command}, $_->{data})));
           shift @yaml_rep;
           $_->{data} = $self->_to_html(join("\n", @yaml_rep));
        }
        $_;
     } @{ $ret->{data}->{report} };

   my $report = {
      id      => $ret->{data}->{_id},
      task    => $ret->{data}->{task},
      service => $ret->{data}->{service},
      dt      => DateTime->from_epoch(epoch => $ret->{data}->{date}),
      parts   => [ @parts ],
   };

   if(scalar(@parts) == 0) {
      $report->{no_detail_view} = 1;
   }

   $self->stash("server", $server);
   $self->stash("report", $report);

   $self->render;
}

sub delete_report {
   my ($self) = @_;
   my $ret = $self->rexio->call(DELETE => "1.0", "report", $self->param("report_id") => undef);

   return $self->render(json => $ret);
}

sub __register__ {
   my ($self, $routes) = @_;
   my $r      = $routes->{route};
   my $r_auth = $routes->{route_auth};

   $r_auth->get("/report/:server_id/:report_id")->to("report#view");
   $r_auth->delete("/report/:server_id/:report_id")->to("report#delete_report");
}

sub __init__ {
   my ($class, $app) = @_;

   # add plugin template path
   push(@{ $app->renderer->paths }, dirname(__FILE__) . "/templates");
   push(@{ $app->static->paths }, dirname(__FILE__) . "/public");
}

sub _to_html {
   my ($self, $str) = @_;

   $str =~ s/</&lt;/gms;
   $str =~ s/>/&gt;/gms;
   $str =~ s/  /&nbsp;&nbsp;&nbsp;/gms;
   $str =~ s/\r?\n/<br \/>/gms;

   return $str;
}

sub _custom_format {
   my ($self, $type, $data) = @_;
   
   my @ret;
   my @_d = @{ $data };
   if($type eq "file" || $type eq "mkdir") {
      push @ret, shift @_d;
      push @ret, { @_d };
   }
   elsif($type eq "chown" || $type eq "chgrp") {
      push @ret, shift @_d;
      push @ret, shift @_d;
      push @ret, { @_d };
   } 
   else {
      return $data;
   }

   return \@ret;
}

1;
