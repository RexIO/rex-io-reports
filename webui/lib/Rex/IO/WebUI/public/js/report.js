function report_prepare_information_view() {
   $(".lnk_report").click(function(event) {
      var srv_id = $(this).attr("srv_id");
      var report_id = $(this).attr("report_id");
      load_page("/report/" + srv_id + "/" + report_id, null, function() {
         report_view({
            "server": srv_id,
            "report": {
               "id": report_id,
            }
         });
      });
   });
}

function report_view(ref) {
   $("#lnk_report_back_to_server").click(function(event) {
      event.preventDefault();
      load_server(ref.server);
   });

   $("#lnk_report_back_to_server_bc").click(function(event) {
      event.preventDefault();
      load_server(ref.server);
   });

   $("#lnk_report_delete").click(function(event) {
      event.preventDefault();
      delete_report(ref.server, ref.report.id);
   });
}

function delete_report(srv_id, id) {
      dialog_confirm({
      id: "report_delete_dialog",
      title: "Really delete this report?",
      text: "This will delete this report. Are you sure?",
      button: "Delete",
      ok: function() {
         $.ajax({
            "url": "/report/" + srv_id + "/" + id,
            "type": "DELETE",
         }).done(function(data) {

            $.pnotify({
               text: "Report deleted",
               type: "info"
            });

	    load_server(srv_id);
         });
      },
      cancel: function() {}
   });
}
